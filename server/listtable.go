package server

import (
	"ujian-bravass/model"
	"ujian-bravass/service"
)

func tableMahasiswa() {
	service.CreateTable(model.TBMahasiswa(), model.ColDocMahasiswa())
}

func tableDosen() {
	service.CreateTable(model.TBDosen(), model.ColDocDosen())
}

func tablePegawai() {
	service.CreateTable(model.TBPegawai(), model.ColDocPegawai())
}

func tableMatkul() {
	service.CreateTable(model.TBMatkul(), model.ColDocMatkul())
}

func tableNilai() {
	service.CreateTable(model.TBNilai(), model.ColDocNilai())

}
