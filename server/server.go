package server

import (
	"log"
	"net/http"
	"ujian-bravass/handler"
	"ujian-bravass/service"
)

const (
	dbHost = "localhost"
	dbPort = 5432
	dbUser = "postgres"
	dbPass = "admin1234"
	dbName = "ujian"
)

// On untuk menyalakan web server
func On() {
	db, err := service.Connect(dbHost, dbPort, dbUser, dbPass, dbName)
	if err != nil {
		log.Fatal(err)
	}

	tableMahasiswa()
	tableDosen()
	tablePegawai()
	tableMatkul()
	tableNilai()

	http.HandleFunc("/api/", handler.SS)
	defer db.Close()

	log.Println("localhost:8090")
	http.ListenAndServe(":8090", nil)
}
