package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"ujian-bravass/service"
)

func handlerDosenGet(w http.ResponseWriter, r *http.Request, page, limit int, col, sort string) {
	last := lastIndex(r)
	if last == "dosen" {
		data, err := service.TampilSemuaDataDosen(page, limit, col, sort)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		jsonData, _ := json.Marshal(data)
		w.Write(jsonData)
	} else {
		temp := service.Dosen{KD: last}
		data, err := temp.TampilSatuData()
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		jsonData, _ := json.Marshal(data)
		w.Write(jsonData)
	}
}

func handlerDosenPost(w http.ResponseWriter, r *http.Request) {
	last := lastIndex(r)
	if last != "dosen" {
		http.Error(w, "Message : error endpoint", http.StatusBadRequest)
		return
	}

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var data service.Dosen
	if err := json.Unmarshal(body, &data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := data.TambahData(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(jsonData)
}

func handlerDosenPut(w http.ResponseWriter, r *http.Request) {
	last := lastIndex(r)
	if last == "dosen" {
		http.Error(w, "Message : error endpoint", http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var data service.Dosen
	if err := json.Unmarshal(body, &data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if last != data.KD {
		http.Error(w, "Message : error endpoint", http.StatusBadRequest)
		return
	}

	if err := data.EditData(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(jsonData)
}

func handlerDosenDelete(w http.ResponseWriter, r *http.Request) {
	last := lastIndex(r)
	if last != "dosen" {
		data := service.Dosen{KD: last}
		if err := data.HapusData(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write([]byte(`{"Message": "Data has been deleted"}`))
	} else {
		http.Error(w, "Message : error endpoint", http.StatusBadRequest)
		return
	}
}
