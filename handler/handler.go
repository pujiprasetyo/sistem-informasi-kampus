package handler

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"ujian-bravass/model"
)

func lastIndex(r *http.Request) string {
	dataURL := strings.Split(fmt.Sprintf("%s", r.URL.Path), "/")
	lastIndex := dataURL[len(dataURL)-1]
	return lastIndex
}

func pagination(w http.ResponseWriter, r *http.Request, colPrimary string) (page, limit int, col, sort string) {
	page = 1
	limit = 3
	col = colPrimary
	sort = "DESC"

	// untuk yg pagination
	// http://localhost:8090/api/mahasiswa?range={[1,3]}
	// if _, ok := r.URL.Query()["range"]; ok {
	// 	rangePage := r.URL.Query()["range"][0]
	// 	var arr []int
	// 	err := json.Unmarshal([]byte(rangePage[1:len(rangePage)-1]), &arr)
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	page = arr[0]
	// 	limit = arr[1]
	// }

	if _, ok := r.URL.Query()["page"]; ok {
		paramPage, err := strconv.Atoi(r.URL.Query()["page"][0])
		if err != nil {
			page = -1
		}
		page = paramPage
	}

	if _, ok := r.URL.Query()["limit"]; ok {
		paramLimit, err := strconv.Atoi(r.URL.Query()["limit"][0])
		if err != nil {
			limit = -1
		}
		limit = paramLimit
	}

	if _, ok := r.URL.Query()["col"]; ok {
		paramCol := r.URL.Query()["col"][0]
		col = paramCol
	}

	if _, ok := r.URL.Query()["sort"]; ok {
		paramSort := r.URL.Query()["sort"][0]
		sort = paramSort
	}

	return page, limit, col, sort
}

// SS untuk mengarahkan func sesuai path endpoint
func SS(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset-utf8; application/json")
	/*** untuk set CORS Origin ***/
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "DELETE, POST, GET, PUT, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	}
	/*** batas CORS Origin ***/

	dataURL := strings.Split(r.URL.Path, "/")
	switch dataURL[2] {
	case "mahasiswa":
		switch r.Method {
		case http.MethodGet:
			page, limit, col, sort := pagination(w, r, model.MahasiswaKey())
			handlerMahasiswaGet(w, r, page, limit, col, sort)
		case http.MethodPost:
			handlerMahasiswaPost(w, r)
		case http.MethodPut:
			handlerMahasiswaPut(w, r)
		case http.MethodDelete:
			handlerMahasiswaDelete(w, r)
		default:
			http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
	case "dosen":
		switch r.Method {
		case http.MethodGet:
			page, limit, col, sort := pagination(w, r, model.DosenKey())
			handlerDosenGet(w, r, page, limit, col, sort)
		case http.MethodPost:
			handlerDosenPost(w, r)
		case http.MethodPut:
			handlerDosenPut(w, r)
		case http.MethodDelete:
			handlerDosenDelete(w, r)
		default:
			http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
	case "pegawai":
		switch r.Method {
		case http.MethodGet:
			page, limit, col, sort := pagination(w, r, model.PegawaiKey())
			handlerPegawaiGet(w, r, page, limit, col, sort)
		case http.MethodPost:
			handlerPegawaiPost(w, r)
		case http.MethodPut:
			handlerPegawaiPut(w, r)
		case http.MethodDelete:
			handlerPegawaiDelete(w, r)
		default:
			http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
	case "matkul":
		switch r.Method {
		case http.MethodGet:
			page, limit, col, sort := pagination(w, r, model.MatkulKey())
			handlerMatkulGet(w, r, page, limit, col, sort)
		case http.MethodPost:
			handlerMatkulPost(w, r)
		case http.MethodPut:
			handlerMatkulPut(w, r)
		case http.MethodDelete:
			handlerMatkulDelete(w, r)
		default:
			http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
	case "nilai":
		switch r.Method {
		case http.MethodGet:
			page, limit, col, sort := pagination(w, r, model.NilaiKey())
			handlerNilaiGet(w, r, page, limit, col, sort)
		case http.MethodPost:
			handlerNilaiPost(w, r)
		case http.MethodPut:
			handlerNilaiPut(w, r)
		case http.MethodDelete:
			handlerNilaiDelete(w, r)
		default:
			http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
	default:
		http.Error(w, "Page Not Found", http.StatusNotFound)
		return
	}
}
