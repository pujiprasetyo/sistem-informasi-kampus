package model

// TBDosen = Table Dosen
func TBDosen() string {
	return "tb_dosen"
}

// DosenKey = Primary Key
func DosenKey() string {
	return "kd"
}

// DocDosen = jsonb dosen
func DocDosen() string {
	return "doc_dosen"
}

// ColDocDosen = jsonb dosen
func ColDocDosen() string {
	return "doc_dosen jsonb NOT NULL"
}
