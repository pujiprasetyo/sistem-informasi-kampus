package model

// TBNilai = table nilai
func TBNilai() string {
	return "tb_nilai"
}

// NilaiKey = primary key
func NilaiKey() string {
	return "npm"
}

// DocNilai = jsonb nilai
func DocNilai() string {
	return "doc_nilai"
}

// ColDocNilai = jsonb nilai
func ColDocNilai() string {
	return "doc_nilai jsonb NOT NULL"
}
