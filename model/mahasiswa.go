package model

// TBMahasiswa = table mahasiswa
func TBMahasiswa() string {
	return "tb_mahasiswa"
}

// MahasiswaKey = primary key
func MahasiswaKey() string {
	return "npm"
}

// DocMahasiswa = jsonb mahasiswa
func DocMahasiswa() string {
	return "doc_mahasiswa"
}

// ColDocMahasiswa = jsonb mahasiswa
func ColDocMahasiswa() string {
	return "doc_mahasiswa jsonb NOT NULL"
}
