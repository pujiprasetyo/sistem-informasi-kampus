package model

// TBMatkul = table matkul
func TBMatkul() string {
	return "tb_matkul"
}

// MatkulKey = primary key
func MatkulKey() string {
	return "kodemk"
}

// DocMatkul = jsonb matkul
func DocMatkul() string {
	return "doc_matkul"
}

// ColDocMatkul = jsonb matkul
func ColDocMatkul() string {
	return "doc_matkul jsonb NOT NULL"
}
