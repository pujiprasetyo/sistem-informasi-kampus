package model

// TBPegawai = table pegawai
func TBPegawai() string {
	return "tb_pegawai"
}

// PegawaiKey = primary key
func PegawaiKey() string {
	return "kp"
}

// DocPegawai = jsonb pegawai
func DocPegawai() string {
	return "doc_pegawai"
}

// ColDocPegawai = jsonb pegawai
func ColDocPegawai() string {
	return "doc_pegawai jsonb NOT NULL"
}
