package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"ujian-bravass/model"

	"gopkg.in/go-playground/validator.v10"
)

// Matkul = struktur data matkul
type Matkul struct {
	KodeMK     string `json:"kodemk" validate:"required,min=9,max=9"`
	MataKuliah string `json:"matakuliah" validate:"required,min=2,max=60"`
}

// Scan ya
func (m *Matkul) Scan(src interface{}) error {
	bs, ok := src.([]byte)
	if !ok {
		return errors.New("not a []byte")
	}
	return json.Unmarshal(bs, m)
}

// TampilSemuaDataMatkul ya
func TampilSemuaDataMatkul(page, limit int, col, sort string) ([]*Matkul, error) {
	result := []*Matkul{}
	offset := limit * (page - 1)
	rows, err := selectAllPaginationJSON(model.TBMatkul(), model.DocMatkul(), offset, limit, col, sort)
	if err != nil {
		return nil, fmt.Errorf("Page Not Found")
	}

	for rows.Next() {
		var hasil Matkul
		err = rows.Scan(&hasil)
		result = append(result, &hasil)
	}
	return result, err
}

// TampilSatuData ya
func (m *Matkul) TampilSatuData() (Matkul, error) {
	var hasil Matkul
	if m.KodeMK == "" {
		return hasil, fmt.Errorf("Page Not Found")
	}

	checks, err := checkData(model.TBMatkul(), model.DocMatkul(), model.MatkulKey(), m.KodeMK)
	if err != nil {
		fmt.Println("checks :", checks)
		fmt.Println("err : ", err)
		return hasil, fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return hasil, fmt.Errorf("Page Not Found")
	}

	rows, err := selectOneJSON(model.TBMatkul(), model.DocMatkul(), model.MatkulKey(), m.KodeMK)
	for rows.Next() {
		err = rows.Scan(&hasil)
	}
	return hasil, err
}

// TambahData ya
func (m *Matkul) TambahData() error {
	validate = validator.New()
	err := validate.Struct(m)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBMatkul(), model.DocMatkul(), model.MatkulKey(), m.KodeMK)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count > 0 {
		return fmt.Errorf(m.KodeMK + " Already Exists")
	}

	jsonData, err := json.Marshal(m)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = insertDataJSON(model.TBMatkul(), jsonData)
	if err != nil {
		return fmt.Errorf("ini apaan si")
	}
	return err
}

// EditData ya
func (m *Matkul) EditData() error {
	validate = validator.New()
	err := validate.Struct(m)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBMatkul(), model.DocMatkul(), model.MatkulKey(), m.KodeMK)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(m.KodeMK + " Not Found")
	}

	jsonData, err := json.Marshal(m)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = updateDataJSON(model.TBMatkul(), model.DocMatkul(), jsonData, model.MatkulKey(), m.KodeMK)
	return err
}

// HapusData ya
func (m *Matkul) HapusData() error {
	checks, err := checkData(model.TBMatkul(), model.DocMatkul(), model.MatkulKey(), m.KodeMK)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(m.KodeMK + " Not Found")
	}

	err = deleteDataJSON(model.TBMatkul(), model.DocMatkul(), model.MatkulKey(), m.KodeMK)
	return err
}
