package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"ujian-bravass/model"

	"gopkg.in/go-playground/validator.v10"
)

// Nilai = struktur data nilai
type Nilai struct {
	NPM   string          `json:"npm" validate:"required,min=8,max=8,alphanum"`
	Nama  string          `json:"nama" validate:"required,min=2,max=60"`
	Kelas string          `json:"kelas" validate:"required,min=5,max=5,alphanum"`
	Nilai []PerMataKuliah `json:"nilai" validate:"required,dive"`
}

// PerMataKuliah = struktur data mata kuliah
type PerMataKuliah struct {
	MataKuliah string `json:"matakuliah" validate:"required,min=2,max=60"`
	UTS        int    `json:"uts" validate:"required,min=0,max=100,numeric"`
	UAS        int    `json:"uas" validate:"required,min=0,max=100,numeric"`
}

// Scan untuk melakukan pemindaian key pada hasil query
func (n *Nilai) Scan(src interface{}) error {
	bs, ok := src.([]byte)
	if !ok {
		return errors.New("not a []byte")
	}
	return json.Unmarshal(bs, n)
}

// TampilSemuaDataNilai untuk menampilkan semua data nilai mahasiswa
func TampilSemuaDataNilai(page, limit int, col, sort string) ([]*Nilai, error) {
	result := []*Nilai{}
	offset := limit * (page - 1)
	rows, err := selectAllPaginationJSON(model.TBNilai(), model.DocNilai(), offset, limit, col, sort)
	if err != nil {
		return nil, fmt.Errorf("Page Not Found")
	}

	for rows.Next() {
		var hasil Nilai
		err = rows.Scan(&hasil)
		result = append(result, &hasil)
	}
	return result, err
}

// TampilSatuData untuk menampilkan satu bio mahasiswa
func (n *Nilai) TampilSatuData() (Nilai, error) {
	var hasil Nilai
	if n.NPM == "" {
		return hasil, fmt.Errorf("Page Not Found")
	}

	checks, err := checkData(model.TBNilai(), model.DocNilai(), model.NilaiKey(), n.NPM)
	if err != nil {
		return hasil, fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return hasil, fmt.Errorf("Page Not Found")
	}

	rows, err := selectOneJSON(model.TBNilai(), model.DocNilai(), model.NilaiKey(), n.NPM)
	for rows.Next() {
		err = rows.Scan(&hasil)
	}
	return hasil, err
}

// TambahData untuk menambah bio mahasiswa
func (n *Nilai) TambahData() error {
	validate = validator.New()
	err := validate.Struct(n)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBNilai(), model.DocNilai(), model.NilaiKey(), n.NPM)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count > 0 {
		return fmt.Errorf(n.NPM + " Already Exists")
	}

	jsonData, err := json.Marshal(n)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = insertDataJSON(model.TBNilai(), jsonData)
	if err != nil {
		return fmt.Errorf("ini apaan si")
	}
	return err
}

// EditData untuk mengedit bio mahasiswa
func (n *Nilai) EditData() error {
	validate = validator.New()
	err := validate.Struct(n)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBNilai(), model.DocNilai(), model.NilaiKey(), n.NPM)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(n.NPM + " Not Found")
	}

	jsonData, err := json.Marshal(n)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = updateDataJSON(model.TBNilai(), model.DocNilai(), jsonData, model.NilaiKey(), n.NPM)
	return err
}

// HapusData untuk mengahapus bio mahasiswa
func (n *Nilai) HapusData() error {
	checks, err := checkData(model.TBNilai(), model.DocNilai(), model.NilaiKey(), n.NPM)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(n.NPM + " Not Found")
	}

	err = deleteDataJSON(model.TBNilai(), model.DocNilai(), model.NilaiKey(), n.NPM)
	return err
}
