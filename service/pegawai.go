package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"ujian-bravass/model"

	"gopkg.in/go-playground/validator.v10"
)

// Pegawai = struktur data pegawai
type Pegawai struct {
	KP   string `json:"kp" validate:"required,min=6,max=6,alphanum"`
	Nama string `json:"nama" validate:"required,min=2,max=60"`
}

// Scan ya
func (p *Pegawai) Scan(src interface{}) error {
	bs, ok := src.([]byte)
	if !ok {
		return errors.New("not a []byte")
	}
	return json.Unmarshal(bs, p)
}

// TampilSemuaDataPegawai ya
func TampilSemuaDataPegawai(page, limit int, col, sort string) ([]*Pegawai, error) {
	result := []*Pegawai{}
	offset := limit * (page - 1)
	rows, err := selectAllPaginationJSON(model.TBPegawai(), model.DocPegawai(), offset, limit, col, sort)
	if err != nil {
		return nil, fmt.Errorf("Page Not Found")
	}

	for rows.Next() {
		var hasil Pegawai
		err = rows.Scan(&hasil)
		result = append(result, &hasil)
	}
	return result, err
}

// TampilSatuData ya
func (p *Pegawai) TampilSatuData() (Pegawai, error) {
	var hasil Pegawai
	if p.KP == "" {
		return hasil, fmt.Errorf("Page Not Found")
	}

	checks, err := checkData(model.TBPegawai(), model.DocPegawai(), model.PegawaiKey(), p.KP)
	if err != nil {
		return hasil, fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return hasil, fmt.Errorf("Page Not Found")
	}

	rows, err := selectOneJSON(model.TBPegawai(), model.DocPegawai(), model.PegawaiKey(), p.KP)
	for rows.Next() {
		err = rows.Scan(&hasil)
	}
	return hasil, err
}

// TambahData ya
func (p *Pegawai) TambahData() error {
	validate = validator.New()
	err := validate.Struct(p)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBPegawai(), model.DocPegawai(), model.PegawaiKey(), p.KP)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count > 0 {
		return fmt.Errorf(p.KP + " Already Exists")
	}

	jsonData, err := json.Marshal(p)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = insertDataJSON(model.TBPegawai(), jsonData)
	if err != nil {
		return fmt.Errorf("ini apaan si")
	}
	return err
}

// EditData ya
func (p *Pegawai) EditData() error {
	validate = validator.New()
	err := validate.Struct(p)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBPegawai(), model.DocPegawai(), model.PegawaiKey(), p.KP)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(p.KP + " Not Found")
	}

	jsonData, err := json.Marshal(p)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = updateDataJSON(model.TBPegawai(), model.DocPegawai(), jsonData, model.PegawaiKey(), p.KP)
	return err
}

// HapusData ya
func (p *Pegawai) HapusData() error {
	checks, err := checkData(model.TBPegawai(), model.DocPegawai(), model.PegawaiKey(), p.KP)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(p.KP + " Not Found")
	}

	err = deleteDataJSON(model.TBPegawai(), model.DocPegawai(), model.PegawaiKey(), p.KP)
	return err
}
