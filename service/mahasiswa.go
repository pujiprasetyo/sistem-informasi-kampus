package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"ujian-bravass/model"

	"gopkg.in/go-playground/validator.v10"
)

// Mahasiswa = struktur data mahasiswa
type Mahasiswa struct {
	NPM   string `json:"npm" validate:"required,min=8,max=8,alphanum"`
	Nama  string `json:"nama" validate:"required,min=2,max=60"`
	Kelas string `json:"kelas" validate:"required,min=5,max=5,alphanum"`
}

// Scan ya
func (m *Mahasiswa) Scan(src interface{}) error {
	bs, ok := src.([]byte)
	if !ok {
		return errors.New("not a []byte")
	}
	return json.Unmarshal(bs, m)
}

// TampilSemuaDataMahasiswa ya
// func TampilSemuaDataMahasiswa(page, limit int, col, sort string) ([]*Mahasiswa, error) {
func TampilSemuaDataMahasiswa(page, limit int, col, sort string) (map[string]interface{}, error) {
	result := []*Mahasiswa{}
	offset := limit * (page - 1)
	rows, err := selectAllPaginationJSON(model.TBMahasiswa(), model.DocMahasiswa(), offset, limit, col, sort)
	if err != nil {
		return nil, fmt.Errorf("Page Not Found")
	}

	for rows.Next() {
		var hasil Mahasiswa
		err = rows.Scan(&hasil)
		result = append(result, &hasil)
	}

	checks, err := dataCount(model.TBMahasiswa(), model.DocMahasiswa())
	if err != nil {
		return nil, fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}

	jsonMap := make(map[string]interface{})
	jsonMap["numberOfRows"] = count
	jsonMap["dataTable"] = result

	return jsonMap, err
}

// TampilSatuData ya
func (m *Mahasiswa) TampilSatuData() (Mahasiswa, error) {
	var hasil Mahasiswa
	if m.NPM == "" {
		return hasil, fmt.Errorf("Page Not Found")
	}

	checks, err := checkData(model.TBMahasiswa(), model.DocMahasiswa(), model.MahasiswaKey(), m.NPM)
	if err != nil {
		return hasil, fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return hasil, fmt.Errorf("Page Not Found")
	}

	rows, err := selectOneJSON(model.TBMahasiswa(), model.DocMahasiswa(), model.MahasiswaKey(), m.NPM)
	for rows.Next() {
		err = rows.Scan(&hasil)
	}
	return hasil, err
}

// TambahData ya
func (m *Mahasiswa) TambahData() error {
	validate = validator.New()
	err := validate.Struct(m)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBMahasiswa(), model.DocMahasiswa(), model.MahasiswaKey(), m.NPM)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count > 0 {
		return fmt.Errorf(m.NPM + " Already Exists")
	}

	jsonData, err := json.Marshal(m)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = insertDataJSON(model.TBMahasiswa(), jsonData)
	if err != nil {
		return fmt.Errorf("ini apaan si")
	}
	return err
}

// EditData ya
func (m *Mahasiswa) EditData() error {
	validate = validator.New()
	err := validate.Struct(m)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBMahasiswa(), model.DocMahasiswa(), model.MahasiswaKey(), m.NPM)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(m.NPM + " Not Found")
	}

	jsonData, err := json.Marshal(m)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = updateDataJSON(model.TBMahasiswa(), model.DocMahasiswa(), jsonData, model.MahasiswaKey(), m.NPM)
	return err
}

// HapusData ya
func (m *Mahasiswa) HapusData() error {
	checks, err := checkData(model.TBMahasiswa(), model.DocMahasiswa(), model.MahasiswaKey(), m.NPM)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(m.NPM + " Not Found")
	}

	err = deleteDataJSON(model.TBMahasiswa(), model.DocMahasiswa(), model.MahasiswaKey(), m.NPM)
	return err
}
