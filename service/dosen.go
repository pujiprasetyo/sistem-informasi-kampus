package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"ujian-bravass/model"

	"gopkg.in/go-playground/validator.v10"
)

// Dosen = data struktur untuk biodata dosen
type Dosen struct {
	KD   string `json:"kd" validate:"required,min=4,max=4,alphanum"`
	Nama string `json:"nama" validate:"required,min=2,max=60"`
}

var validate *validator.Validate

// Scan ya
func (d *Dosen) Scan(src interface{}) error {
	bs, ok := src.([]byte)
	if !ok {
		return errors.New("not a []byte")
	}
	return json.Unmarshal(bs, d)
}

// TampilSemuaDataDosen ya
func TampilSemuaDataDosen(page, limit int, col, sort string) ([]*Dosen, error) {
	result := []*Dosen{}
	offset := limit * (page - 1)
	rows, err := selectAllPaginationJSON(model.TBDosen(), model.DocDosen(), offset, limit, col, sort)
	if err != nil {
		return nil, fmt.Errorf("Page Not Found")
	}

	for rows.Next() {
		var hasil Dosen
		err = rows.Scan(&hasil)
		result = append(result, &hasil)
	}
	return result, err
}

// TampilSatuData ya
func (d *Dosen) TampilSatuData() (Dosen, error) {
	var hasil Dosen
	if d.KD == "" {
		return hasil, fmt.Errorf("Page Not Found")
	}

	checks, err := checkData(model.TBDosen(), model.DocDosen(), model.DosenKey(), d.KD)
	if err != nil {
		return hasil, fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return hasil, fmt.Errorf("Page Not Found")
	}

	rows, err := selectOneJSON(model.TBDosen(), model.DocDosen(), model.DosenKey(), d.KD)
	for rows.Next() {
		err = rows.Scan(&hasil)
	}
	return hasil, err
}

// TambahData ya
func (d *Dosen) TambahData() error {
	validate = validator.New()
	err := validate.Struct(d)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBDosen(), model.DocDosen(), model.DosenKey(), d.KD)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count > 0 {
		return fmt.Errorf(d.KD + " Already Exists")
	}

	jsonData, err := json.Marshal(d)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = insertDataJSON(model.TBDosen(), jsonData)
	if err != nil {
		return fmt.Errorf("ini apaan si")
	}
	return err
}

// EditData ya
func (d *Dosen) EditData() error {
	validate = validator.New()
	err := validate.Struct(d)
	if err != nil {
		var pesanError string
		for _, err := range err.(validator.ValidationErrors) {
			pesanError = "Periksa kembali " + err.Field() + " \n" + pesanError
		}
		return fmt.Errorf(pesanError)
	}

	checks, err := checkData(model.TBDosen(), model.DocDosen(), model.DosenKey(), d.KD)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(d.KD + " Not Found")
	}

	jsonData, err := json.Marshal(d)
	if err != nil {
		return fmt.Errorf("Internal Server Error")
	}
	err = updateDataJSON(model.TBDosen(), model.DocDosen(), jsonData, model.DosenKey(), d.KD)
	return err
}

// HapusData ya
func (d *Dosen) HapusData() error {
	checks, err := checkData(model.TBDosen(), model.DocDosen(), model.DosenKey(), d.KD)
	if err != nil {
		return fmt.Errorf("Page Not Found")
	}
	var count int
	for checks.Next() {
		err = checks.Scan(&count)
	}
	if count < 1 {
		return fmt.Errorf(d.KD + " Not Found")
	}

	err = deleteDataJSON(model.TBDosen(), model.DocDosen(), model.DosenKey(), d.KD)
	return err
}
