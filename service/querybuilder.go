package service

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	// Driver postgresql
	_ "github.com/lib/pq"
)

var dbQueryBuilder *sql.DB

// Connect ya
func Connect(dbHost string, dbPort int, dbUser, dbPass, dbName string) (*sql.DB, error) {
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPass, dbName)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping()
	dbQueryBuilder = db
	return db, err
}

// CreateTable ya
func CreateTable(tableName string, column ...string) error {
	columns := strings.Join(column, ", ")
	query := "CREATE TABLE " + tableName + " (" + columns + ");"

	_, err := dbQueryBuilder.Exec(query)
	return err
}

// func selectAllPagination(tableName string, offset, limit int, col, sort string) (*sql.Rows, error) {
// 	query := "SELECT * FROM " + tableName + " ORDER BY " + col + " " + sort + " OFFSET $1 LIMIT $2;"

// 	rows, err := dbQueryBuilder.Query(query, offset, limit)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return rows, err
// }

// func selectOne(tableName, column string, value ...*string) error {
// 	query := "SELECT * FROM " + tableName + " WHERE " + column + " = '" + *value[0] + "';"
// 	switch len(value) {
// 	case 2:
// 		return dbQueryBuilder.QueryRow(query).Scan(value[0], value[1])
// 	case 3:
// 		return dbQueryBuilder.QueryRow(query).Scan(value[0], value[1], value[2])
// 	default:
// 		return dbQueryBuilder.Ping()
// 	}
// }

// func insertData(tableName string, value ...string) error {
// 	values := strings.Join(value, "', '")
// 	query := "INSERT INTO " + tableName + " VALUES ('" + values + "');"

// 	_, err := dbQueryBuilder.Exec(query)
// 	return err
// }

// func updateData(tableName, column, value, keyItem, item string) error {
// 	query := "UPDATE " + tableName + " SET " + keyItem + " = '" + item + "' WHERE " + column + " = '" + value + "';"

// 	_, err := dbQueryBuilder.Exec(query)
// 	return err
// }

// func deleteData(tableName, column, value string) error {
// 	query := "DELETE FROM " + tableName + " WHERE " + column + " = '" + value + "';"

// 	_, err := dbQueryBuilder.Exec(query)
// 	return err
// }

/*****************/
/*** Coba JSONB ***/
/*****************/

func selectAllPaginationJSON(tableName, docName string, offset, limit int, col, sort string) (*sql.Rows, error) {
	query := "SELECT * FROM " + tableName + " ORDER BY " + docName + " ->> '" + col + "' " + sort + " OFFSET $1 LIMIT $2;"

	rows, err := dbQueryBuilder.Query(query, offset, limit)
	if err != nil {
		return nil, err
	}
	return rows, err
}

func selectOneJSON(tableName, docName, column, value string) (*sql.Rows, error) {
	query := "SELECT " + docName + " FROM " + tableName + " WHERE " + docName + " ->> '" + column + "' = '" + value + "';"
	rows, err := dbQueryBuilder.Query(query)
	if err != nil {
		return nil, err
	}
	return rows, err
}

func checkData(tableName, docName, column, value string) (*sql.Rows, error) {
	query := "SELECT count(" + docName + ") FROM " + tableName + " WHERE " + docName + " ->> '" + column + "' = '" + value + "';"
	rows, err := dbQueryBuilder.Query(query)
	if err != nil {
		return nil, err
	}
	return rows, err
}

func insertDataJSON(tableName string, json []byte) error {
	query := "INSERT INTO " + tableName + " VALUES ('" + string(json) + "');"

	_, err := dbQueryBuilder.Exec(query)
	return err
}

func updateDataJSON(tableName, docName string, json []byte, column, value string) error {
	query := "UPDATE " + tableName + " SET " + docName + " = " + docName + " || '" + string(json) + "' WHERE " + docName + " ->> '" + column + "' = '" + value + "';"

	_, err := dbQueryBuilder.Exec(query)
	return err
}

func deleteDataJSON(tableName, docName, column, value string) error {
	query := "DELETE FROM " + tableName + " WHERE " + docName + " ->> '" + column + "' = '" + value + "';"

	_, err := dbQueryBuilder.Exec(query)
	return err
}

func dataCount(tableName, docName string) (*sql.Rows, error) {
	query := "SELECT count(" + docName + ") FROM " + tableName + " ;"
	rows, err := dbQueryBuilder.Query(query)
	if err != nil {
		return nil, err
	}
	return rows, err
}
