# Sistem Informasi Kampus

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Web server REST API ini sebagai backend yang akan memproduce json yang di butuhkan untuk frontend. Aplikasi ini dibuat dengan bahasa pemrograman GoLang. Golang merupakan bahasa pemrograman dari google. Bahasa pemrograman tersebut sangat ringkas, terdapat fungsi multiple return dan aplikasi tidak akan bisa running bila ada variable yang unused.

  - Golang
  - Database Postgresql

# Features!

  - Terdapat beberapa entitas yakni mahasiswa, dosen, pegawai, mata kuliah dan nilai mahasiswa
  - Semua entitas dapat melakukan proses CRUD
  - Terdapat fitur pagination and sortir
  - Sisi database tidak menggunakan struktur data, karena postgresql telah terdapat type data jsonb. Type data tersebut bisa digunakan seperti database non relasional

Berikut cara pemanggilan API dengan salah satu entitas

GET (Menampilkan semua data dengan pagination & sortir)
```
url : localhost:8090/mahasiswa?col=npm&sort=asc&page=1&limit=5
```

GET (Menampilkan satu data mahasiswa)
```
url : localhost:8090/mahasiswa/12345678
```

POST
```
url : localhost:8090/mahasiswa
body :
{
    "npm": "12345678"
    "nama": "Go Lang"
    "kelas": "1IT01"
}
```

PUT (Last url diberi npm yang akan di update)
```
url : localhost:8090/mahasiswa/12345678
body :
{
    "npm": "12345678"
    "nama": "Go Lang"
    "kelas": "1IK09"
}
```

DELETE (Last url diberi npm yang akan di delete)
```
url : localhost:8090/mahasiswa/12345678
```